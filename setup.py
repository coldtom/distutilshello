from setuptools import setup

setup(
      name='distutilshello',
      version='0.0.0',
      description='Says hello world',
      author='Thomas Coldrick',
      license='MIT',
      packages=['hello'],
      entry_points = {
          'console_scripts': ['hello = hello.hello:hello']
      },
      include_package_data=True,
)
